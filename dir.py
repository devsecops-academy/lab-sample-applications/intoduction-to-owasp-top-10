import requests
base_url = '<BASE_URL>'
directories = open('<PATH_FILE>')
for line in directories:
    path = line.strip()
    url = base_url+path
    response = requests.get(url, allow_redirects=False)
    if "<MISSING_DATA>" not in str(response.text):
        print(url,' - Success')  
    else:
        print(url,' - Failed')
